#build empty floppy disk and add bootloader
nasm bootload.asm
dd if=/dev/zero of=floppya.img bs=512 count=2880
dd if=bootload of=floppya.img bs=512 count=1 conv=notrunc
#compile kernel.c and link kernel.asm
bcc -ansi -c -o kernel.o kernel.c
as86 kernel.asm -o kernel_asm.o
ld86 -o kernel -d kernel.o kernel_asm.o
dd if=kernel of=floppya.img bs=512 conv=notrunc seek=259
#Lab 3 - Adds the cat message file to sector 30 on floppy
dd if=msg of=floppya.img bs=512 count=1 seek=30 conv=notrunc
#config creation
#dd if=floppy.img of=config bs=512 skip=258 count=1 conv=notrunc

#run bochs
#bochs -f osxterm.txt