/* ACADEMIC INTEGRITY PLEDGE                                              */
/*                                                                        */
/* - I have not used source code obtained from another student nor        */
/*   any other unauthorized source, either modified or unmodified.        */
/*                                                                        */
/* - All source code and documentation used in my program is either       */
/*   my original work or was derived by me from the source code           */
/*   published in the textbook for this course or presented in            */
/*   class.                                                               */
/*                                                                        */
/* - I have not discussed coding details about this project with          */
/*   anyone other than my instructor. I understand that I may discuss     */
/*   the concepts of this program with other students and that another    */
/*   student may help me debug my program so long as neither of us        */
/*   writes anything during the discussion or modifies any computer       */
/*   file during the discussion.                                          */
/*                                                                        */
/* - I have violated neither the spirit nor letter of these restrictions. */
/*                                                                        */
/*                                                                        */
/*                                                                        */
/* Signed:Jacob Date: 2/7/2018        */
/*                                                                        */
/*                                                                        */
/* 3460:4/526 BlackDOS2020 kernel, Version 1.01, Spring 2018.             */

void handleInterrupt21(int,int,int,int);
void printString(char* c, int d);
void printLogo();
void readString(char *cStr);
void readInt(int *n);
void writeInt(int x);
int mod(int a, int b);
int div(int a, int b);
int strToInt(char *cStr);
void intToString(int num, char* cStr);
void readSector(char* buffer, int sector);
void writeSector(char* buffer, int sector);
void clearScreen(int bgColor, int fgColor);
void error(int err);
void readFile(char *fname, char *buffer, int *size);
void deleteFile(char *fname);
void writeFile(char *fname, char *buffer, int *size);
void runProgram(char* name, int segment);
void stop();

// void madlib();

void main()
{	
	char buffer[512];
	makeInterrupt21();
	interrupt(33,2,buffer,258,0);
	interrupt(33,12,buffer[0]+1,buffer[1]+1,0);
	printLogo();
	// interrupt(33,4,"kitty1\0",2,0);
	interrupt(33,4,"fib\0",2,0);
	interrupt(33,5,0,0,0);
	interrupt(33,0,"Error if this executes.\r\n\0",0,0);
}

void printString(char* c, int d)
{
   char *al;
   char ah;
   int ax;
   al = c;
   ah = 14;
   ax = 0;
   while(*al != '\0')
   {
      if(d == 0)
      {
         ax = (ah * 256) + *al;
         interrupt(16, ax, 0, 0, 0);
      }else if(d = 1)
      {
         interrupt(23, *al, 0, 0, 0);
      }
      al++;
   }

   return;
}

void printLogo()
{
   printString("       ___   `._   ____  _            _    _____   ____   _____ \r\n\0",0);
   printString("      /   \\__/__> |  _ \\| |          | |  |  __ \\ / __ \\ / ____|\r\n\0",0);
   printString("     /_  \\  _/    | |_) | | __ _  ___| | _| |  | | |  | | (___ \r\n\0",0);
   printString("    // \\ /./      |  _ <| |/ _` |/ __| |/ / |  | | |  | |\\___ \\ \r\n\0",0);
   printString("   //   \\\\        | |_) | | (_| | (__|   <| |__| | |__| |____) |\r\n\0",0);
   printString("._/'     `\\.      |____/|_|\\__,_|\\___|_|\\_\\_____/ \\____/|_____/\r\n\0",0);
   printString(" BlackDOS2020 v. 1.01, c. 2018. Based on a project by M. Black. \r\n\0",0);
   printString(" Author: Jacob Armbruster.\r\n\r\n\0",0);
   return;
}

/* MAKE FUTURE UPDATES HERE */
/* VVVVVVVVVVVVVVVVVVVVVVVV */



void runProgram(char* name, int segment)
{
	int* segCount;
	int baseLocation = 4096 * segment; //4096 is equal to 0x1000
	char buffer[12288]; //(max block size * max filesize)
	int i = 0;
	readFile(name, buffer, segCount);
	//putInMemory(intbaseLocation, int offset, char c)
	for(i = 0; i < *segCount * 512; i++)
		putInMemory(baseLocation, i, buffer[i]);

	launchProgram(baseLocation);

}

void stop()
{
	while(1)
		;
}


void error(int err)
{
	switch(err)
	{
		//output error to screen
		case 0:
			printString("File not found.\r\n\0", 0); break;
		case 1:
			printString("Duplicate or invalid filename.\r\n\0", 0); break;
		case 2:
			printString("Disk full.\r\n\0", 0); break;
		default:
			printString("General error\r\n\0", 0); break;
		//hang
	}
	while(1);
}

void readFile(char *fname, char *buffer, int *size)
{
	char directory[512];
	char num[8];
	int i, k;
	int match = -1;
	readSector(directory, 257); //257 is disk directory
	//for each file name
	for(i = 0; i < 512; i += 32)
	{
		//check if filename matches
		for(k = 0; k < 8; k++)
		{
			if(*(fname + k) != *(directory +  i + k))
			{
				break;
			}
			if(k == 7) //if all chars matched
			{
				match = i;
			}
		}
		if(match != -1)
		{
			break;
		}
	}

	if(match == -1)
	{
		error(0); //File not found error
	}

	i = match + 8; //move up one byte
	k = 0;
	while(*(directory + i) != 0 && k != 23)
	{
		readSector(buffer, *(directory + i));
		buffer += 512;
		k++;
		i++;
	}
	*size = k;

	return;
}

void writeFile(char *fname, char *buffer, int *size)
{
	char directory[512];
	char map[512];
	int filenameLoc = 512;
	int i, k;

	readSector(map, 256);
	readSector(directory, 257);


	//find free filename location and check if a file already exists
	for(i = 0; i < 512; i += 32)
	{
		//check if file is free
		if(i < filenameLoc && *(directory + i) == '\0')
			filenameLoc = i;

		//check if filename matches
		for(k = 0; k < 8; k++)
		{
			if(*(fname + k) != *(directory + i + k))
			{
				break;
			}
			//if all 8 chars matched or
			//if filename and directory's filename's characters are both \0
			if(k == 7 || (*(fname + k) == 0 && *(directory + i + k) == 0))
			{
				error(1); //Duplicate file error
			}
		}
	}

	//write the filename to that freespace
	i = 0;
	while(i < 8)
	{
		if(*(fname + i) != '\0')
			*(directory + filenameLoc + i) = *(fname + i);
		else
			break;
		i++;
	}
	while(i < 8)
	{
		*(directory + filenameLoc + i) = '\0';
		i++;
	}

	//write provided buffer to 'size' sectors
	filenameLoc += 8;
	k = 1;
	for(i = 0; i < size; i++)
	{
		while(k < 255) //sectors are between 01 and FE
		{
			//if map location doesnt equal ff
			if(*(map + k) != 255)
			{
				//reserve space in map
				*(map + k) = 255;
				//write buffer to that sector
				writeSector((buffer + (i * 512)), k);
				//put sector number into disk direcstory
				*(directory + filenameLoc + i) = k;
				k++;
				break;
			}
			k++;
		}
		if(k == 512)
			error(2); //disk full
	}

	//Fill the unused sector slots with 0's in the directory
	while(i < 24)
	{
		*(directory + filenameLoc + i) = '\0';
		i++;
	}

	writeSector(map, 256);
	writeSector(directory, 257);

	return;
}

void deleteFile(char *fname)
{
	char directory[512];
	char map[512];
	int sectorLoc = 512;
	int i, k;

	readSector(map, 256);
	readSector(directory, 257);


	//find filename to delete file or throw error
	for(i = 0; i < 512; i += 32)
	{
		//check if filename matches
		for(k = 0; k < 8; k++)
		{
			if(*(fname + k) != *(directory + i + k))
			{
				break;
			}
			//if all 8 chars matched or if filename and directory's filename's
			//characters  are both \0. Then the file name was found.
			if(k == 7 || (*(fname + k) == 0 && *(directory + i + k) == 0))
			{
				//set first byte of directory filename to 0
				*(directory + i) = 0;

				//for each sector set it's map byte to 0;
				sectorLoc = i + 8;
				while(*(directory + sectorLoc) != 0 || *(directory + sectorLoc) == 24)
				{
					*(map + *(directory + sectorLoc)) = 0;
					sectorLoc++;
				}

				writeSector(map, 256);
				writeSector(directory, 257);

				return;
			}
		}
	}

	error(0); //File not found

	return;
}


void readSector(char* buffer, int sector)
{
	//Lab 3 contains formulas for these 6 variables.
	int relSecNo = mod(sector, 18) + 1;
	int headNo = mod(div(sector, 18), 2);
	int trackNo = div(sector, 36);
	int ax = 513; // 513 = ((AH=2) << 8) + (AL = 1)
	int cx = (trackNo << 8) + relSecNo;
	int dx = headNo << 8;
	interrupt(19,ax, buffer, cx, dx);
	return;
}

void writeSector(char* buffer, int sector)
{
	//Lab 3 contains formulas for these 6 variables.
	int relSecNo = mod(sector, 18) + 1;
	int headNo = mod(div(sector, 18), 2);
	int trackNo = div(sector, 36);
	int ax = 769; // 769 = ((AH=6) << 8) + (AL = 1)
	int cx = (trackNo << 8) + relSecNo;
	int dx = headNo << 8;
	interrupt(19,ax, buffer, cx, dx);
	return;
}

void clearScreen(int bgColor, int fgColor)
{
	//Issue 24 carriage return/newline combos
	int i;
	for(i = 0; i < 24; i++)
	{
		printString("\r\n\0", 0);
	}
	//Place cursor in upper left hand corner (coords [0,0])
	interrupt(16,512,0,0,0);
	//Scroll the screen with attributes pertaining to color
	if(bgColor > 0  && bgColor <= 8 && fgColor > 0 && fgColor <= 16)
	{
		interrupt(16,1536,4096 * (bgColor - 1) + ((fgColor - 1) << 8), 0, 6223);
	}
}

// void madlib()
// {
//    char food[25], adjective[25], color[25], animal[25];
//    int temp;
//    makeInterrupt21();
//    printLogo();
//    interrupt(33,0,"\r\nWelcome to the Mad Libs kernel.\r\n\0",0,0);
//    interrupt(33,0,"Enter a food: \0",0,0);
//    interrupt(33,1,food,0,0);
//    temp = 0;
//    while ((temp < 100) || (temp > 120)) {
//       interrupt(33,0,"Enter a number between 100 and 120: \0",0,0);
//       interrupt(33,14,&temp,0,0);
//    }
//    interrupt(33,0,"Enter an adjective: \0",0,0);
//    interrupt(33,1,adjective,0,0);
//    interrupt(33,0,"Enter a color: \0",0,0);
//    interrupt(33,1,color,0,0);
//    interrupt(33,0,"Enter an animal: \0",0,0);
//    interrupt(33,1,animal,0,0);
//    interrupt(33,0,"Your note is on the printer, go get it.\r\n\0",0,0);
//    interrupt(33,0,"Dear Professor O\'Neil,\r\n\0",1,0);
//    interrupt(33,0,"\r\nI am so sorry that I am unable to turn in my program at this time.\r\n\0",1,0);
//    interrupt(33,0,"First, I ate a rotten \0",1,0);
//    interrupt(33,0,food,1,0);
//    interrupt(33,0,", which made me turn \0",1,0);
//    interrupt(33,0,color,1,0);
//    interrupt(33,0," and extremely ill.\r\n\0",1,0);
//    interrupt(33,0,"I came down with a fever of \0",1,0);
//    interrupt(33,13,temp,1,0);
//    interrupt(33,0,". Next my \0",1,0);
//    interrupt(33,0,adjective,1,0);
//    interrupt(33,0," pet \0",1,0);
//    interrupt(33,0,animal,1,0);
//    interrupt(33,0," must have\r\nsmelled the remains of the \0",1,0);
//    interrupt(33,0,food,1,0);
//    interrupt(33,0," on my computer, because he ate it. I am\r\n\0",1,0);
//    interrupt(33,0,"currently rewriting the program and hope you will accept it late.\r\n\0",1,0);
//    interrupt(33,0,"\r\nSincerely,\r\n\0",1,0);
//    interrupt(33,0,"Jacob Armbruster\r\n\0",1,0);
//    while(1);
// }

void readString(char *cStr)
{
	// char cStr[80];
	unsigned int k = 0;
	char c;
	char line[3];
	line[0] = '\r';
	line[1] = '\n';
	line[2] = '\0';
	while(1)
	{
		//Interupt 22 accepts keybodard input when ah = 0
		c = interrupt(22,0,0,0,0);
		//ASCII 8 is backspace.
		if(c == 8 && k != 0)
		{
			k--;
			continue;
		}
		//ASCII 13(0xD) is enter.
		if(c == 13)
			break;
		//If at max character length replace last character with new character.
		//79 is one less than the 80 declared for the c string.
		if(k == 79)
			k--;

		*(cStr + k) = c;
		k++;
	}
	*(cStr + k) = '\0';
	interrupt(33,0,cStr,0,0);
	interrupt(33,0,line,0,0);

	//for some reason without the use of an array the newline won't print...
	// interrupt(33,0,"\r\n\0",0,0); 
	return;
}

void readInt(int *n)
{
	char c[80];
	readString(c);
	*n = strToInt(c);
	return;
}

void writeInt(int x)
{
	char c[8];
	intToString(x, c);
	printString(c, 0);
	return;
}

void intToString(int num, char* cStr)
{
	int i = 0, k = 0;
	int isNeg = 0;
	char buffer[8]; 
	//longest string needed is -32768 then a '/0'

	if(num < 0)
	{
		num *= -1; //make it positive
		isNeg = 1;
	}

	while(num != 0)
	{

		buffer[i] = (char) ((mod(num, 10)) + '0');
		num /= 10;
		i++;
	}

	//add negtive to cStr if needed
	if(isNeg)
	{
		*(cStr) = '-';
		k++;
	}
	//reverse buffer into actual c string
	while(i != 0)
	{
		i--;
		*(cStr + k) = buffer[i];
		k++;
	}

	*(cStr + k) = '\0';

	return;
}

int strToInt(char *cStr)
{
	int i = 0;
	int num = 0;
	int length = 0;
	int isNeg = 0;
	while(*(cStr + i) != '\0')
		i++;
	length = i;
	i = 0;

	if(*(cStr) == '-')
	{
		isNeg = 1;
		i++;
	}


	while(i < length)
	{
		num *= 10;
		num += *(cStr + i) - '0';
		i++;
		// the "- '0'" portion maps the values from ASCII
	}

	if(isNeg)
	{
		num *= -1;
	}

	return num;
}

int mod(int a, int b)
{
	int x = a;
	while(x >= b)
		x = x - b;
	return x;
}

int div(int a, int b)
{
	int q = 0;
	while(q * b <= a)
		q++;
	return (q-1);

}
/* ^^^^^^^^^^^^^^^^^^^^^^^^ */
/* MAKE FUTURE UPDATES HERE */

void handleInterrupt21(int ax, int bx, int cx, int dx)
{
/*   return; */
   switch(ax) { 
      case 0: printString(bx,cx); break; 
      case 1: readString(bx); break;
      case 2: readSector(bx,cx); break;
      case 3: readFile(bx,cx,dx); break;
      case 4: runProgram(bx,cx); break;
      case 5: stop(); break;
      case 6: writeSector(bx,cx); break;
      case 7: deleteFile(bx); break;
      case 8: writeFile(bx,cx,dx); break;
      case 12: clearScreen(bx, cx); break;
      case 13: writeInt(bx); break;
      case 14: readInt(bx); break;
      case 15: error(bx); break;
/*      case 1: case 2: case 3: case 4: case 5: */
/*      case 6: case 7: case 8: case 9: case 10: */
/*      case 11: case 12: case 13: case 14: case 15: */
      default: printString("General BlackDOS error.\r\n\0");
  }
}
