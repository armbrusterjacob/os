#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> // for sleep
#include <time.h>
#include <pthread.h>
#include <semaphore.h>

enum ingredient
{
	in_tobacco,
	in_paper,
	in_match,
};

void smokerTobaccoThread();
void smokerPaperThread();
void smokerMatchThread();
void pusherTobaccoThread();
void pusherPaperThread();
void pusherMatchThread();
void agentThread();

sem_t sem_agent;
sem_t sem_to;
sem_t sem_pa;
sem_t sem_ma;
sem_t sem_push_to;
sem_t sem_push_pa;
sem_t sem_push_ma;
sem_t mutex;
int count_to;
int count_pa;
int count_ma;

int main()
{
	// Initialize variables
	sem_init(&sem_agent, 0, 1);	
	sem_init(&sem_to, 0, 0);	
	sem_init(&sem_pa, 0, 0);	
	sem_init(&sem_ma, 0, 0);	
	sem_init(&sem_push_to, 0, 0);	
	sem_init(&sem_push_pa, 0, 0);	
	sem_init(&sem_push_ma, 0, 0);	
	sem_init(&mutex, 0, 1);	
	count_to = count_pa = count_ma = 0;
	pthread_t pt_smoker_to[2];
	pthread_t pt_smoker_pa[2];
	pthread_t pt_smoker_ma[2];
	pthread_t pt_pusher_to;
	pthread_t pt_pusher_pa;
	pthread_t pt_pusher_ma;
	pthread_t pt_agent[3];

	// Start POSIX theads
	for(int i = 0; i < 2; i++)
	{
		pthread_create(&pt_smoker_to[i], NULL, (void *) smokerTobaccoThread, NULL);
		pthread_create(&pt_smoker_pa[i], NULL, (void *) smokerPaperThread, NULL);
		pthread_create(&pt_smoker_ma[i], NULL, (void *) smokerMatchThread, NULL);
	}
	pthread_create(&pt_pusher_to, NULL, (void *) pusherTobaccoThread, NULL);
	pthread_create(&pt_pusher_pa, NULL, (void *) pusherPaperThread, NULL);
	pthread_create(&pt_pusher_ma, NULL, (void *) pusherMatchThread, NULL);
	for(int i = 0; i < 3; i++)
	{	
		pthread_create(&pt_agent[i], NULL, (void *) agentThread, NULL);
	}

	// Destory threads, and mutexes
	for(int i = 0; i < 2; i++)
	{
		pthread_join(pt_smoker_to[i], NULL);
		pthread_join(pt_smoker_pa[i], NULL);
		pthread_join(pt_smoker_ma[i], NULL);
	}
	pthread_join(pt_pusher_to, NULL);
	pthread_join(pt_pusher_pa, NULL);
	pthread_join(pt_pusher_ma, NULL);
	for(int i = 0; i < 3; i++)
	{
		pthread_join(pt_agent[i], NULL);
	}

	sem_destroy(&sem_agent);
	sem_destroy(&sem_to);
	sem_destroy(&sem_pa);	
	sem_destroy(&sem_ma);
	sem_destroy(&sem_push_to);
	sem_destroy(&sem_push_pa);	
	sem_destroy(&sem_push_ma);
	sem_destroy(&mutex);
}

void smokerTobaccoThread()
{
	srand(time(NULL)); // seed rand with current time
	while(1)
	{
		sem_wait(&sem_to);
		// Creating cigarette...
		int random_wait = (rand() % 25000000) + 250000000; // Random time between 25ms and 50ms
		nanosleep((const struct timespec[]){{0, random_wait}}, NULL);
		// Created cigarette!
		sem_post(&sem_agent);
		// Smoking cigarette...
		nanosleep((const struct timespec[]){{0, random_wait}}, NULL);
		// Finished smoking.
		printf("  Smoker with tobacco smoked.\n");
	}
}
void smokerPaperThread()
{
	while(1)
	{
		sem_wait(&sem_pa);
		// Creating cigarette...
		int random_wait = (rand() % 25000000) + 250000000; // Random time between 25ms and 50ms
		nanosleep((const struct timespec[]){{0, random_wait}}, NULL);
		// Created cigarette!
		sem_post(&sem_agent);
		// Smoking cigarette...
		nanosleep((const struct timespec[]){{0, random_wait}}, NULL);
		// Finished smoking.
		printf("  Smoker with paper smoked.\n");
	}
}
void smokerMatchThread()
{
	while(1)
	{
		sem_wait(&sem_ma);
		// Creating cigarette...
		int random_wait = (rand() % 25000000) + 250000000; // Random time between 25ms and 50ms
		nanosleep((const struct timespec[]){{0, random_wait}}, NULL);
		// Created cigarette!
		sem_post(&sem_agent);
		// Smoking cigarette...
		nanosleep((const struct timespec[]){{0, random_wait}}, NULL);
		// Finished smoking.
		printf("  Smoker with matches smoked.\n");
	}
}

void pusherTobaccoThread()
{
	for(int i = 0; i < 12; i++)
	{
		sem_wait(&sem_push_to);
		sem_wait(&mutex);
		if(count_pa > 0)
		{
			count_pa--;
			sem_post(&sem_ma);
		}else if(count_ma > 0)
		{
			count_ma--;
			sem_post(&sem_pa);
		}
		else
			count_to++;
		sem_post(&mutex);
	}
}

void pusherPaperThread()
{
	for(int i = 0; i < 12; i++)
	{
		sem_wait(&sem_push_pa);
		sem_wait(&mutex);
		if(count_ma > 0)
		{
			count_ma--;
			sem_post(&sem_to);
		}else if(count_to > 0)
		{
			count_to--;
			sem_post(&sem_ma);
		}
		else
			count_pa++;
		sem_post(&mutex);
	}
}

void pusherMatchThread()
{
	for(int i = 0; i < 12; i++)
	{
		sem_wait(&sem_push_ma);
		sem_wait(&mutex);
		if(count_pa > 0)
		{
			count_pa--;
			sem_post(&sem_to);
		}else if(count_to > 0)
		{
			count_to--;
			sem_post(&sem_pa);
		}
		else
			count_ma++;
		sem_post(&mutex);
	}
}

void agentThread()
{
	srand(time(NULL)); // seed rand with current time
	//randomly choose 2 different preset ingredients
	int previousChosen[2];
	previousChosen[0] = rand() % 3;
	previousChosen[1] = rand() % 3;
	while(previousChosen[1] != previousChosen[0])
		previousChosen[1] = rand() % 3;

	for(int i = 0; i < 6; i++)
	{
		sem_wait(&sem_agent);
		int notChosen;
		if(previousChosen[0] == in_tobacco || previousChosen[1] == in_tobacco)
		{
			if(previousChosen[0] == in_match || previousChosen[1] == in_match)
			{
				notChosen =  in_paper;
			}
			else
				notChosen = in_match;
		}
		else 
			notChosen = in_tobacco

		if(notChosen == in_tobacco)
		{
			printf("[Agent distributing match and paper...]\n");
			sem_post(&sem_push_ma);
			sem_post(&sem_push_pa);
		}else if(notChosen == in_paper)
		{
			printf("[Agent distributing match and tobacco...]\n");
			sem_post(&sem_push_ma);
			sem_post(&sem_push_to);
		}else
		{
			printf("[Agent distributing tobacco and paper...]\n");
			sem_post(&sem_push_to);
			sem_post(&sem_push_pa);
		}
		
		int random_wait = (rand() % 100000000) + 100000000; // Random time between 100ms and 200ms
		nanosleep((const struct timespec[]){{0, random_wait}}, NULL);
	}
}