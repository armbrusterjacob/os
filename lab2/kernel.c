﻿/* ACADEMIC INTEGRITY PLEDGE                                              */
/*                                                                        */
/* - I have not used source code obtained from another student nor        */
/*   any other unauthorized source, either modified or unmodified.        */
/*                                                                        */
/* - All source code and documentation used in my program is either       */
/*   my original work or was derived by me from the source code           */
/*   published in the textbook for this course or presented in            */
/*   class.                                                               */
/*                                                                        */
/* - I have not discussed coding details about this project with          */
/*   anyone other than my instructor. I understand that I may discuss     */
/*   the concepts of this program with other students and that another    */
/*   student may help me debug my program so long as neither of us        */
/*   writes anything during the discussion or modifies any computer       */
/*   file during the discussion.                                          */
/*                                                                        */
/* - I have violated neither the spirit nor letter of these restrictions. */
/*                                                                        */
/*                                                                        */
/*                                                                        */
/* Signed:Jacob Date: 2/7/2018        */
/*                                                                        */
/*                                                                        */
/* 3460:4/526 BlackDOS2020 kernel, Version 1.01, Spring 2018.             */

void handleInterrupt21(int,int,int,int);
void printLogo();
void readString(char *cStr);
void readInt(int *n);
void writeInt(int x);
int mod(int a, int b);
int div(int a, int b);
int strToInt(char *cStr);
void intToString(int num, char* cStr);
void madlib();

void main()
{
	madlib();

   // char c[80];
   // int n = 0;
   // int p = &n;

   // makeInterrupt21();
   // printLogo();

   // interrupt(33,0,"Enter an intger. (16-bit)\r\n\0",0,0);
   // interrupt(33,14,p,0,0); //readInt(n)
   // interrupt(33,13,n,0,0); //writeInt(n)

   while(1);
}

void printString(char* c, int d)
{
   char *al;
   char ah;
   int ax;
   al = c;
   ah = 14;
   ax = 0;
   while(*al != '\0')
   {
      if(d == 0)
      {
         ax = (ah * 256) + *al;
         interrupt(16, ax, 0, 0, 0);
      }else if(d = 1)
      {
         interrupt(23, *al, 0, 0, 0);
      }
      al++;
   }

   return;
}

void printLogo()
{
   printString("       ___   `._   ____  _            _    _____   ____   _____ \r\n\0",0);
   printString("      /   \\__/__> |  _ \\| |          | |  |  __ \\ / __ \\ / ____|\r\n\0",0);
   printString("     /_  \\  _/    | |_) | | __ _  ___| | _| |  | | |  | | (___ \r\n\0",0);
   printString("    // \\ /./      |  _ <| |/ _` |/ __| |/ / |  | | |  | |\\___ \\ \r\n\0",0);
   printString("   //   \\\\        | |_) | | (_| | (__|   <| |__| | |__| |____) |\r\n\0",0);
   printString("._/'     `\\.      |____/|_|\\__,_|\\___|_|\\_\\_____/ \\____/|_____/\r\n\0",0);
   printString(" BlackDOS2020 v. 1.01, c. 2018. Based on a project by M. Black. \r\n\0",0);
   printString(" Author: Jacob Armbruster.\r\n\r\n\0",0);
   return;
}

/* MAKE FUTURE UPDATES HERE */
/* VVVVVVVVVVVVVVVVVVVVVVVV */

void madlib()
{
   char food[25], adjective[25], color[25], animal[25];
   int temp;
   makeInterrupt21();
   printLogo();
   interrupt(33,0,"\r\nWelcome to the Mad Libs kernel.\r\n\0",0,0);
   interrupt(33,0,"Enter a food: \0",0,0);
   interrupt(33,1,food,0,0);
   temp = 0;
   while ((temp < 100) || (temp > 120)) {
      interrupt(33,0,"Enter a number between 100 and 120: \0",0,0);
      interrupt(33,14,&temp,0,0);
   }
   interrupt(33,0,"Enter an adjective: \0",0,0);
   interrupt(33,1,adjective,0,0);
   interrupt(33,0,"Enter a color: \0",0,0);
   interrupt(33,1,color,0,0);
   interrupt(33,0,"Enter an animal: \0",0,0);
   interrupt(33,1,animal,0,0);
   interrupt(33,0,"Your note is on the printer, go get it.\r\n\0",0,0);
   interrupt(33,0,"Dear Professor O\'Neil,\r\n\0",1,0);
   interrupt(33,0,"\r\nI am so sorry that I am unable to turn in my program at this time.\r\n\0",1,0);
   interrupt(33,0,"First, I ate a rotten \0",1,0);
   interrupt(33,0,food,1,0);
   interrupt(33,0,", which made me turn \0",1,0);
   interrupt(33,0,color,1,0);
   interrupt(33,0," and extremely ill.\r\n\0",1,0);
   interrupt(33,0,"I came down with a fever of \0",1,0);
   interrupt(33,13,temp,1,0);
   interrupt(33,0,". Next my \0",1,0);
   interrupt(33,0,adjective,1,0);
   interrupt(33,0," pet \0",1,0);
   interrupt(33,0,animal,1,0);
   interrupt(33,0," must have\r\nsmelled the remains of the \0",1,0);
   interrupt(33,0,food,1,0);
   interrupt(33,0," on my computer, because he ate it. I am\r\n\0",1,0);
   interrupt(33,0,"currently rewriting the program and hope you will accept it late.\r\n\0",1,0);
   interrupt(33,0,"\r\nSincerely,\r\n\0",1,0);
   interrupt(33,0,"Jacob Armbruster\r\n\0",1,0);
   while(1);
}

void readString(char *cStr)
{
	// char cStr[80];
	unsigned int k = 0;
	char c;
	while(1)
	{
		//Interupt 22 accepts keybodard input when ah = 0
		c = interrupt(22,0,0,0,0);
		//ASCII 8 is backspace.
		if(c == 8 && k != 0)
		{
			k--;
			continue;
		}
		//ASCII 13(0xD) is enter.
		if(c == 13)
			break;
		//If at max character length replace last character with new character.
		//79 is one less than the 80 declared for the c string.
		if(k == 79)
			k--;

		*(cStr + k) = c;
		k++;
	}
	*(cStr + k) = '\0';
	// interrupt(33,0,cStr,0,0); 
	return;
}

void readInt(int *n)
{
	char c[80];
	char *p = &c;
	readString(p);
	*n = strToInt(p);
	return;
}

void writeInt(int x)
{
	char c[8];
	intToString(x, c);
	printString(c, 1);
	return;
}

void intToString(int num, char* cStr)
{
	int i = 0, k = 0;
	int isNeg = 0;
	char buffer[8]; 
	//longest string needed is -32768 then a '/0'

	if(num < 0)
	{
		num *= -1; //make it positive
		isNeg = 1;
	}

	while(num != 0)
	{

		buffer[i] = (char) ((mod(num, 10)) + '0');
		num /= 10;
		i++;
	}

	//add negtive to cStr if needed
	if(isNeg)
	{
		*(cStr) = '-';
		k++;
	}
	//reverse buffer into actual c string
	while(i != 0)
	{
		i--;
		*(cStr + k) = buffer[i];
		k++;
	}

	*(cStr + k) = '\0';

	return;
}

int strToInt(char *cStr)
{
	int i = 0;
	int num = 0;
	int length = 0;
	int isNeg = 0;
	while(*(cStr + i) != '\0')
		i++;
	length = i;
	i = 0;

	if(*(cStr) == '-')
	{
		isNeg = 1;
		i++;
	}


	while(i < length)
	{
		num *= 10;
		num += *(cStr + i) - '0';
		i++;
		// the "- '0'" portion maps the values from ASCII
	}

	if(isNeg)
	{
		num *= -1;
	}

	return num;
}

int mod(int a, int b)
{
	int x = a;
	while(x >= b)
		x = x - b;
	return x;
}

int div(int a, int b)
{
	int q = 0;
	while(q * b <= a)
		q++;
	return (q-1);

}
/* ^^^^^^^^^^^^^^^^^^^^^^^^ */
/* MAKE FUTURE UPDATES HERE */

void handleInterrupt21(int ax, int bx, int cx, int dx)
{
/*   return; */
   switch(ax) { 
      case 0: printString(bx,cx); break; 
      case 1: readString(bx); break;
      case 13: writeInt(bx); break;
      case 14: readInt(bx); break;
/*      case 1: case 2: case 3: case 4: case 5: */
/*      case 6: case 7: case 8: case 9: case 10: */
/*      case 11: case 12: case 13: case 14: case 15: */
      default: printString("General BlackDOS error.\r\n\0");
  }
}
