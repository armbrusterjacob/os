// ACADEMIC INTEGRITY PLEDGE
//
// - I have not used source code obtained from another student nor
//   any other unauthorized source, either modified or unmodified.
//
// - All source code and documentation used in my program is either
//   my original work or was derived by me from the source code
//   published in the textbook for this course or presented in
//   class.
//
// - I have not discussed coding details about this project with
//   anyone other than my instructor. I understand that I may discuss
//   the concepts of this program with other students and that another
//   student may help me debug my program so long as neither of us
//   writes anything during the discussion or modifies any computer
//   file during the discussion.
//
// - I have violated neither the spirit nor letter of these restrictions.
//
//
//
// Signed:Jacob Armbruster Date: 4/8/2018

// 3460:426 Lab 4A - Basic C shell

/* Basic shell */

/*
 * This is a very minimal shell. It finds an executable in the
 * PATH, then loads it and executes it (using execv). Since
 * it uses "." (dot) as a separator, it cannot handle file
 * names like "minishell.h"
 *
 * The focus on this exercise is to use fork, PATH variables,
 * and execv. 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include<sys/wait.h>

#define MAX_ARGS	64
#define MAX_ARG_LEN	16
#define MAX_LINE_LEN	80
#define WHITESPACE	" ,\t\n"

struct command_t {
   char *name;
   int argc;
   char *argv[MAX_ARGS];
};

/* Function prototypes */
int parseCommand(char *, struct command_t *);
void printPrompt();
void readCommand(char *);

int main(int argc, char *argv[]) {
   int pid;
   int status;
   char cmdLine[MAX_LINE_LEN];
   struct command_t command;

   while(1) {
      printPrompt();
      /* Read the command line and parse it */
      readCommand(cmdLine);
      
      parseCommand(cmdLine, &command);
      
      command.argv[command.argc] = NULL;
      
      /* Create a child process to execute the command */
      if ((pid = fork()) == 0) {
         /* Child executing command */
        if(strcmp(command.name, "C") == 0)
        {
          command.argv[0] = "cp";
          execvp("cp", command.argv);
        }
        else if(strcmp(command.name, "D") == 0)
        {
          command.argv[0] = "rm";
          execvp("rm", command.argv);
        } 
        else if(strcmp(command.name, "E") == 0)
        {
          if(command.argv[1])
          {
            command.argv[0] = "echo";
            execvp("echo", command.argv);
          }else
          {
            //if the comment field is empty do nothing
            exit(0);
          }
        } 
        else if(strcmp(command.name, "H") == 0)
        {
          printf(" This shell is for a lab for an operating systems class. Users can use the\n");
          printf(" following commands where brackets indicate user input. (ex: [file])\n");
          printf(" \n");
          printf("   C [file1] [file2]\n");
          printf("       Copy the file, [file1] into [file2]\n");
          printf("   D [file]\n");
          printf("       Delte the file, [file].\n");
          printf("   E [comment]\n");
          printf("       Echo the comemnt into the shell. If no comment is made then simply\n");
          printf("       return input\n");
          printf("   H\n");
          printf("       You are here, this is the user manual where you can learn about the\n");
          printf("       functionaly of the shell.\n");
          printf("   L\n");
          printf("       Lists the contents of the current directory and also list it's contents\n");
          printf("   M [file]\n");
          printf("       Creates a new file with the filename, [file].\n");
          printf("   P [file]\n");
          printf("       Print out the file [file] to the shell.\n");
          printf("   Q\n");
          printf("       Exit the shell.\n");
          printf("   S\n");
          printf("       Surf the web with an internet browser.\n");
          printf("   W\n");
          printf("       Clear the screen.\n");
          printf("   X [program]\n");
          printf("       Execute an external program.\n");
          exit(0);
        } 
        else if(strcmp(command.name, "L") == 0)
        {
          int pid2;
          char *args[2];
          args[0] = "pwd";
          args[1] = NULL;
          printf("\n");

          //Spawn a second child process in order to do two system calls
          if((pid2 = fork()) == 0)
          {
            execvp("pwd", args);
            printf("\n");
            exit(0);
          }

          //First child then exexutes the rest
          wait(&status);
          char *args2[3];
          args2[0] = "ls";
          args2[1] = "-l";
          args2[2] = NULL;
          execvp("ls", args2);

        } 
        else if(strcmp(command.name, "M") == 0)
        {
          command.argv[0] = "touch";
          execvp("touch", command.argv);
        } 
        else if(strcmp(command.name, "P") == 0)
        {
          command.argv[0] = "more";
          execvp("more", command.argv);
        } 
        else if(strcmp(command.name, "Q") == 0)
        {
          //The child does nothing, later the parent checks the command's name in order to quit.
          exit(0);
        } 
        else if(strcmp(command.name, "S") == 0)
        {
          int pid2 = 1;
          if((pid2 = fork()) == 0)
          { 
            command.argv[0] = "firefox";
            execvp("firefox", command.argv);
          }
        } 
        else if(strcmp(command.name, "W") == 0)
        {
          command.argv[0] = "clear";
          execvp("clear", command.argv);
        } 
        else if(strcmp(command.name, "X") == 0)
        {
          execvp(command.argv[1], command.argv + 1);
        }
        else
        {
          execvp(command.name, command.argv);
        }

        //Catch any remaining child whose execvp command failed.
        exit(0);
      }


      /* Wait for the child to terminate */
      wait(&status);

      if(strcmp(command.name, "Q") == 0)
        break;
   }

   /* Shell termination */
   printf("\n\n shell: Terminating successfully\n");
   return 0;
}

/* End basic shell */

/* Parse Command function */

/* Determine command name and construct the parameter list.
 * This function will build argv[] and set the argc value.
 * argc is the number of "tokens" or words on the command line
 * argv[] is an array of strings (pointers to char *). The last
 * element in argv[] must be NULL. As we scan the command line
 * from the left, the first token goes in argv[0], the second in
 * argv[1], and so on. Each time we add a token to argv[],
 * we increment argc.
 */
int parseCommand(char *cLine, struct command_t *cmd) {
   int argc;
   char **clPtr;
   /* Initialization */
   clPtr = &cLine;	/* cLine is the command line */
   argc = 0;
   cmd->argv[argc] = (char *) malloc(MAX_ARG_LEN);
   /* Fill argv[] */
   while ((cmd->argv[argc] = strsep(clPtr, WHITESPACE)) != NULL) {
      cmd->argv[++argc] = (char *) malloc(MAX_ARG_LEN);
   }

   /* Set the command name and argc */
   cmd->argc = argc-1;
   cmd->name = (char *) malloc(sizeof(cmd->argv[0]));
   strcpy(cmd->name, cmd->argv[0]);
   return 1;
}

/* End parseCommand function */

/* Print prompt and read command functions - pp. 79-80 */

void printPrompt() {
   /* Build the prompt string to have the machine name,
    * current directory, or other desired information
    */
   char *promptString = "linux-jaa108|>";
   printf("%s ", promptString);
}

void readCommand(char *buffer) {
   /* This code uses any set of I/O functions, such as those in
    * the stdio library to read the entire command line into
    * the buffer. This implementation is greatly simplified,
    * but it does the job.
    */
   fgets(buffer, 80, stdin);
}

/* End printPrompt and readCommand */