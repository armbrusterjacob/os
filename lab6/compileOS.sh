#build empty floppy disk and add bootloader
nasm bootload.asm
dd if=/dev/zero of=floppya.img bs=512 count=2880
dd if=bootload of=floppya.img bs=512 count=1 conv=notrunc
dd if=map of=floppya.img bs=512 count=1 seek=256 conv=notrunc
dd if=config of=floppya.img bs=512 count=1 seek=258 conv=notrunc
#compile kernel.c and link kernel.asm
bcc -ansi -c -o kernel.o kernel.c
as86 kernel.asm -o kernel_asm.o
ld86 -o kernel -d kernel.o kernel_asm.o
#add kernel to floppy
dd if=kernel of=floppya.img bs=512 conv=notrunc seek=259

bcc -ansi -c -o fib.o fib.c
as86 blackdos.asm -o bdos_asm.o
ld86 -o fib -d fib.o bdos_asm.o

bcc -ansi -c -o shell.o shell.c
as86 blackdos.asm -o bdos_asm.o
ld86 -o Shell -d shell.o bdos_asm.o

#lab 5 
./loadFile kitty1
./loadFile kitty2
./loadFile fib
./loadFile Shell

#run bochs
#bochs -f osxterm.txt