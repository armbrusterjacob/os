#define PRINTS(x)  interrupt(33,0,x,0,0)
#define PRINTN(x)  interrupt(33,13,x,0,0)
#define LPRINTS(x) interrupt(33,0,x,1,0)
#define LPRINTN(x) interrupt(33,13,x,1,0)
#define SCANS(x)   interrupt(33,1,x,0,0)
#define SCANN(x)   interrupt(33,14,&x,0,0)
#define END        interrupt(33,5,0,0,0)

void loadConfig();
int strcmp(char*str1,char*str2);
void matchCommand(char *c);
int isCaps(char c);
char* getWord(char *src, char *dst);
int strToInt(char *cStr);

void main()
{
	// loadConfig();
	while(1)
	{
		char input[512];
		PRINTS("blackdos~(__^> \0");
		SCANS(input);
		matchCommand(input);
		// PRINTS("\r\n\0");
	}
}

void matchCommand(char *c)
{
	char command[16];
	char *ptr;
	ptr = getWord(c, command);

	if(!strcmp(command, "boot\0"))
	{
		//boot
		interrupt(25,0,0,0,0);
	}
	else if(!strcmp(command, "cls\0"))
	{
		loadConfig();
	}
	else if(!strcmp(command, "copy\0"))
	{
		if(!isCaps(*(ptr)))
		{
			if(*(ptr) != '\0')
			{
				char buffer[12288];
				int size = 0;
				ptr = getWord(ptr, command);
				// readFile(char *fname, char *buffer, int *size);
				interrupt(33,3,command,buffer,&size);
				ptr = getWord(ptr, command);
				//writeFile(char *fname, char *buffer, int *size);
				interrupt(33, 8, command, buffer, size);
			}
		}
		else
		{
			interrupt(33,15,1,0,0); //Duplicate file err
			// PRINTS("Can't copy system file\r\n\0");
		}
	}
	else if(!strcmp(command, "del\0"))
	{
		ptr = getWord(ptr, command);
		if(!isCaps(*(command)))
			interrupt(33,7,command,0,0);
		else
		{
			PRINTS("Duplicate or invalid filename.\r\n\0");
			// PRINTS("Cannot delete system file.\r\n\0");
		}
	}
	else if(!strcmp(command, "dir\0"))
	{
		char directory[512];
		char temp[9];
		int i, k, total = 0;
		interrupt(33,2,directory,257,0);

		for(i = 0; i < 512; i += 32)
		{
			if(*(directory + i) == '\0')
			{
				continue;
			}

			k = 0;
			while(*(directory + i  + k + 8) != 0)
			{
				if(k == 24)
					break;
				k++;
			}
			total += k;

			if(!isCaps(*(directory + i)))
			{

				PRINTS("s: \0");
				PRINTN(k);
				if(k / 10 == 0)
					PRINTS(" \0");
				else
					PRINTS("  \0");

				for(k = 0; k < 8; k++)
				{
					*(temp + k) = *(directory + i + k);
				}
				*(temp + i + 8) = '\0';
				PRINTS(temp);

				PRINTS("\r\n\0");
			}
		}

		PRINTS("Total blocks used: \0");
		PRINTN(total);
		PRINTS("\r\n\0");
	}
	else if(!strcmp(command, "echo\0"))
	{
		ptr = getWord(ptr, command);
		while(*(command) != '\0')
		{
			PRINTS(command);
			PRINTS(" \0");
			ptr = getWord(ptr, command);
		}
		PRINTS("\r\n\0");
	}
	else if(!strcmp(command, "help\0"))
	{
		PRINTS("OS\r\n\0");
		PRINTS("    Using this OS boots into the shell wherein you can use many commands\r\n\0");
		PRINTS("    listed below.\r\n\0");
		PRINTS("Shell\r\n\0");
		PRINTS("    Starting this OS you will boot into it's shell. From the shell\r\n\0");
		PRINTS("    where you can then use the following commands. (text in [backets]\r\n\0");
		PRINTS("    are provided by the user.):\r\n\0");
		PRINTS("        -boot: Reboot the OS.\r\n\0");
		PRINTS("        -cls: Clear the screen.\r\n\0");
		PRINTS("        -del [filename]: Delete a file and remove it from the directory.\r\n\0");
		PRINTS("        -dir : List all files in the directory and their size.\r\n\0");
		PRINTS("        -echo [comment]: Display the comment given.\r\n\0");
		PRINTS("        -help: Displays a help manual for the OS.\r\n\0");
		PRINTS("        -lprint [filename]: Load a file into memory and print it.\r\n\0");
		PRINTS("        -type [filename]: Load a file into memory and print it.\r\n\0");
		PRINTS("        -run [filename]: Execute a program.\r\n\0");
		PRINTS("        -setenv fg [color (0-15)]:  Set the foreground color for the OS.\r\n\0");
		PRINTS("        -setenv bg [color (0-7)]: Set the background color for the OS.\r\n\0");
		PRINTS("        -setenv default: Set default foreground and backgroun colors\r\n\0");
		PRINTS("        -tweet [filename]: Create a file with 140 characters.\r\n\0");
	}
	else if(!strcmp(command, "lprint\0"))
	{
		if(!isCaps(*(ptr)))
		{
			char buffer[12288];
			int size;
			ptr = getWord(ptr, command);
			//readFile
			interrupt(33,3,command,buffer,&size);
			PRINTS(buffer);
			PRINTS("\r\n\0");
		}
		else
		{
			PRINTS("Can't print system file.\r\n\0");
		}
	}
	else if(!strcmp(command, "type\0"))
	{
		if(!isCaps(*(ptr)))
		{
			char buffer[12288];
			int size;
			ptr = getWord(ptr, command);
			//readFile
			interrupt(33,3,command,buffer,&size);
			PRINTS(buffer);
			PRINTS("\r\n\0");
		}
		else
			PRINTS("Can't print system file.\r\n\0");
	}
	else if(!strcmp(command, "run\0"))
	{
		if(!isCaps(*(ptr)))
		{
			ptr = getWord(ptr, command);
			interrupt(33,4,command,3,0);
		}
		else
			PRINTS("Can't run system file.\r\n\0");
	}
	else if(!strcmp(command, "setenv\0"))
	{
		char buffer[512];
		int color;
		interrupt(33,2,buffer,258,0);

		ptr = getWord(ptr, command);
		if(!strcmp(command, "fg\0"))
		{
			ptr = getWord(ptr, command);
			color = strToInt(command); 
			if(color >= 0 && color <= 15)
			{
				buffer[1] = color;
				interrupt(33,6,buffer,258,0);
				loadConfig();
			}
			else
			{
				PRINTS("Invalid color \r\n\0");
			}
		}
		else if(!strcmp(command, "bg\0"))
		{
			ptr = getWord(ptr, command);
			color = strToInt(command); 
			if(color >= 0 && color <= 7)
			{
				buffer[0] = color;
				interrupt(33,6,buffer,258,0);
				loadConfig();
			}
			else
			{
				PRINTS("Invalid color \r\n\0");
			}
		}
		else if(!strcmp(command, "default\0"))
		{
			buffer[0] = 3;
			buffer[1] = 11;
			interrupt(33,6,buffer,258,0);
			loadConfig();
		}
		else
			PRINTS("Not a valid enviornment variable.\r\n\0");
	}
	else if(!strcmp(command, "tweet\0"))
	{
		ptr = getWord(ptr, command);
		if(!isCaps(*(command)))
		{
			if(*(command) != '\0')
			{
				char buffer[512];
				interrupt(33,1,buffer,0,0);
				interrupt(33, 8, command, buffer, 1);
			}
		}
		else
		{
			PRINTS("Duplicate or invalid filename.\r\n\0");
		}
	}
	else
	{
		PRINTS("Bad command.\r\n\0");
	}

}


int isCaps(char c)
{
	if(c >= 'A' && c <= 'Z')
	{
		return 1;
	}
	return 0;
}

/*
	getWord gets the next word and is delimited by spaces.
		returns a pointer to either the next word or '\0'
*/
char* getWord(char *src, char *dst)
{
	int i = 0;
	while(*(src + i) != '\0' && *(src + i) != ' ')
	{
		*(dst + i) = *(src + i);
		i++;
	}

	*(dst + i) = '\0';
	while(*(src + i) == ' ')
		i++;
	return src + i;
}

void loadConfig()
{
	char buffer[512];
	//load into buffer
	interrupt(33,2,buffer,258,0);
	//Clear screen and set colors
	interrupt(33,12,buffer[0]+1,buffer[1]+1,0);
	return;
}

int strcmp(char *str1, char *str2)
{
	int i = 0;
	while(*(str1 + i) == *(str2 + i))
	{
		if(*(str1 + i) == '\0')
		{
			//they're the same
			return 0;
		}
		i++;
	} 
	if(*(str1 + i) > *(str2 + i))
	{
		return 1;
	}
	else
	{
		return -1;
	}
}

int strToInt(char *cStr)
{
	int i = 0;
	int num = 0;
	int length = 0;
	int isNeg = 0;
	while(*(cStr + i) != '\0')
		i++;
	length = i;
	i = 0;

	if(*(cStr) == '-')
	{
		isNeg = 1;
		i++;
	}


	while(i < length)
	{
		num *= 10;
		num += *(cStr + i) - '0';
		i++;
		// the "- '0'" portion maps the values from ASCII
	}

	if(isNeg)
	{
		num *= -1;
	}

	return num;
}